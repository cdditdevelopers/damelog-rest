package br.com.servicedesk.rest.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(schema = "corporate", name = "invoices")
public class Invoice extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_invoices", sequenceName = "\"corporate\".invoices_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_invoices")
    private Long id;

//    @Column(name = "file", nullable = false)
//    @Lob
//    private byte[] file;

    @Column(name = "number_invoice")
    private String number;

    @Column(name = "due_date")
    private LocalDateTime dueDate;

    @Column(name = "amount", nullable= false, precision=7, scale=2)
    @Digits(integer=10, fraction=2)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private InvoiceStatusEnum status;

    @ManyToOne
    @JoinColumn(name="company_id", nullable=false)
    private Company company;

//    public byte[] getFile() {
//        return file;
//    }

//    public void setFile(byte[] file) {
//        this.file = file;
//    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public InvoiceStatusEnum getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatusEnum status) {
        this.status = status;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
