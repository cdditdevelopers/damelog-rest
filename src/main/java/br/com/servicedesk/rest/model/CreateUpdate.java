package br.com.servicedesk.rest.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Embeddable
public class CreateUpdate {

    //    @UpdateTimestamp ??
    @Column(name = "modification_time")
    protected LocalDateTime updatedOn;

    //    @CreationTimestamp ??
    @Column(name = "creation_time")
    protected LocalDateTime createdAt;

    protected CreateUpdate(){}

    public CreateUpdate(LocalDateTime updatedOn, LocalDateTime createdAt) {
        this.updatedOn = updatedOn;
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
