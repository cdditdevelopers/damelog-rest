package br.com.servicedesk.rest.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "corporate", name = "states")
public class State extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_states", sequenceName = "\"corporate\".states_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_states")
    private Long id;
    private String name;
    private String acronym;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="state")
    private Set<City> cities;

    @ManyToOne
    @JoinColumn(name="country_id")
    private Country country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronyms) {
        this.acronym = acronyms;
    }

    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}


/*

    return queryInterface.bulkInsert('States', [{
      id: 1,
      abbreviation: 'AC',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      abbreviation: 'AL',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      abbreviation: 'AM',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      abbreviation: 'AP',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 5,
      abbreviation: 'BA',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 6,
      abbreviation: 'CE',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 7,
      abbreviation: 'DF',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 8,
      abbreviation: 'ES',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 9,
      abbreviation: 'GO',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 10,
      abbreviation: 'MA',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 11,
      abbreviation: 'MG',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 12,
      abbreviation: 'MS',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 13,
      abbreviation: 'MT',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 14,
      abbreviation: 'PA',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 15,
      abbreviation: 'PB',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 16,
      abbreviation: 'PE',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 17,
      abbreviation: 'PI',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 18,
      abbreviation: 'PR',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 19,
      abbreviation: 'RJ',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 20,
      abbreviation: 'RN',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 21,
      abbreviation: 'RO',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 22,
      abbreviation: 'RR',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 23,
      abbreviation: 'RS',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 24,
      abbreviation: 'SC',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 25,
      abbreviation: 'SE',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 26,
      abbreviation: 'SP',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 27,
      abbreviation: 'TO',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
    // });
  },

  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('States', null, {});
  }
};
 */