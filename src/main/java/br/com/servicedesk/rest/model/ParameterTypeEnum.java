package br.com.servicedesk.rest.model;

public enum ParameterTypeEnum {
    STRING, INTEGER, SQL, BOOLEAN, BIGDECIMAL
}
