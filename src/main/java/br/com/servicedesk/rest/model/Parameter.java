package br.com.servicedesk.rest.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(schema = "corporate", name = "parameters",
uniqueConstraints = @UniqueConstraint(columnNames = "key_param", name = "parameter_key_uk"))
@Cacheable
public class Parameter extends BaseEntity{

    @Id
    @SequenceGenerator(name = "sq_parameters", sequenceName = "\"corporate\".parameters_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_parameters")
    private Long id;

    @NotEmpty
    @Column(name = "value_param")
    private String value;

    @NotEmpty

    @Column(name = "key_param")
    private String key;

    public Parameter() {}

    public Parameter(@NotEmpty String value, @NotEmpty String key) {
        this.value = value;
        this.key = key;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
