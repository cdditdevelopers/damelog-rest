package br.com.servicedesk.rest.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(schema = "corporate", name = "companies")
public class Company extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_companies", sequenceName = "\"corporate\".companies_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_companies")
    private Long id;

    @Column(name="corporate_name")
    private String corporateName;

    @Column(name="cnpj")
    private String cnpj;

    @Column(name="state_registration")
    private String stateRegistration;

    @Column(name="phone")
    private String phone;

    @Column(name="email")
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id") //check user_ir???
    private Customer manager;

//    @OneToMany(mappedBy="company", fetch = FetchType.LAZY)
//    private Set<Customer> customers;

    @OneToMany(mappedBy="company", fetch = FetchType.LAZY)
    private Set<Invoice> invoices;

    @OneToMany(mappedBy="company", fetch = FetchType.LAZY)
    private Set<Address> addresses;

    @Column(name = "amount_monthly_contract", nullable= false, precision=7, scale=2)
    @Digits(integer=10, fraction=2)
    private BigDecimal amountMonthlyContract;

    @Column(name = "contract_date_start")
    private LocalDate contractDateStart;

    @Column(name = "contract_date_end")
    private LocalDate contractDateEnd;

    @Column(name = "business_branch")
    private String businessBranch;

    public Company(Long id) {
        this.id = id;
    }

    public Company(){}


    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getStateRegistration() {
        return stateRegistration;
    }

    public void setStateRegistration(String stateRegistration) {
        this.stateRegistration = stateRegistration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public Set<Customer> getCustomers() {
//        return customers;
//    }
//
//    public void setCustomers(Set<Customer> customers) {
//        this.customers = customers;
//    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }

    public BigDecimal getAmountMonthlyContract() {
        return amountMonthlyContract;
    }

    public void setAmountMonthlyContract(BigDecimal amountMonthlyContract) {
        this.amountMonthlyContract = amountMonthlyContract;
    }

    public LocalDate getContractDateStart() {
        return contractDateStart;
    }

    public void setContractDateStart(LocalDate contractDateStart) {
        this.contractDateStart = contractDateStart;
    }

    public LocalDate getContractDateEnd() {
        return contractDateEnd;
    }

    public void setContractDateEnd(LocalDate contractDateEnd) {
        this.contractDateEnd = contractDateEnd;
    }

    public String getBusinessBranch() {
        return businessBranch;
    }

    public void setBusinessBranch(String businessBranch) {
        this.businessBranch = businessBranch;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getManager() {
        return manager;
    }

    public void setManager(Customer manager) {
        this.manager = manager;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}