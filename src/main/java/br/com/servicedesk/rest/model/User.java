package br.com.servicedesk.rest.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "corporate", name = "users")
@SQLDelete(sql="update \"corporate\".users set is_deleted = true where id = ?")
@Where(clause = "is_deleted = false") //it does not apply to native queries
public class User extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_users", sequenceName = "\"corporate\".users_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_users")
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    private String login;
    private String password;
    @Column(name = "token_expired")
    private boolean tokenExpired;
    private LocalDate birthday;

    @NotEmpty
    @ManyToMany
    @JoinTable(
            name = "users_roles",
            schema = "corporate",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", tokenExpired=" + tokenExpired +
                ", birthday=" + birthday +
                ", roles=" + roles +
                ", version=" + version +
                ", updatedOn=" + createUpdate.updatedOn +
                ", createdAt=" + createUpdate.createdAt +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
