package br.com.servicedesk.rest.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(schema = "corporate", name = "privileges")
public class Privilege extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_privileges", sequenceName = "\"corporate\".privileges_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_privileges")
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}