package br.com.servicedesk.rest.model;

import javax.persistence.*;

@Entity
@Table(schema = "corporate", name = "business_segment")
public class BusinessSegment {

    @Id
    @SequenceGenerator(name = "sq_business_segment", sequenceName = "\"corporate\".business_segment_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_business_segment")
    private Long id;

    private Boolean isDeleted;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
