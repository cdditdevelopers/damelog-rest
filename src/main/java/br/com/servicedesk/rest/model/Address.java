package br.com.servicedesk.rest.model;

import javax.persistence.*;

@Entity
@Table(schema = "corporate", name = "addresses")
public class Address extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_addresses", sequenceName = "\"corporate\".addresses_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_addresses")
    private Long id;

    private String zip;
    private String place;
    private String neighbourhood;
    @Column(name="number_address")
    private String numberAddress;

    @ManyToOne
    @JoinColumn(name="city_id")
    private City city;


    @ManyToOne
    @JoinColumn(name="company_id")
    private Company company;

    @Enumerated(EnumType.STRING)
    @Column(name="address_type")
    private AddressTypeEnum addressType;
    private String complement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getNumberAddress() {
        return numberAddress;
    }

    public void setNumberAddress(String numberAddress) {
        this.numberAddress = numberAddress;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public AddressTypeEnum getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressTypeEnum addressType) {
        this.addressType = addressType;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
