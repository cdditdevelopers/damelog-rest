package br.com.servicedesk.rest.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "corporate", name = "roles")
public class Role extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_roles", sequenceName = "\"corporate\".roles_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_roles")
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @ManyToMany
    @JoinTable(
            name = "roles_privileges",
            schema = "corporate",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "privilege_id", referencedColumnName = "id"))
    private Set<Privilege> privileges;


    public Role(){}

    public Role(Long id){
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privilege> privileges) {
        this.privileges = privileges;
    }
}
