package br.com.servicedesk.rest.model;

public enum InvoiceStatusEnum {
    PAID, OPEN, OVERDUE;
}
