package br.com.servicedesk.rest.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "corporate", name = "countries")
public class Country extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_countries", sequenceName = "\"corporate\".countries_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_countries")
    private Long id;
    private String acronym;
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<State> states;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<State> getStates() {
        return states;
    }

    public void setStates(Set<State> states) {
        this.states = states;
    }
}
