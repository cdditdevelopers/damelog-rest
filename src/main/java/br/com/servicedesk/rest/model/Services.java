package br.com.servicedesk.rest.model;

import javax.persistence.*;

@Entity
@Table(schema = "corporate", name = "services")
public class Services extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_services", sequenceName = "\"corporate\".services_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_services")
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


    /*
        tabela de dominio

        valores: email, banco dedados, middleware, suporte ti 8x5/ ou 24/7, reede dados / redes wireless, segurance de ti, monitorcao
     */
