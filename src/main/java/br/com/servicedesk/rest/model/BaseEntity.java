package br.com.servicedesk.rest.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    //Embddable para version, updateon, createdAt ?
    @Column(nullable = false)
    @Version
    protected Long version = 0L;

    @Embedded
    protected CreateUpdate createUpdate;

    @Column(name = "is_deleted")
    protected Boolean isDeleted;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public LocalDateTime getUpdatedOn() {
        return createUpdate.updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.createUpdate.updatedOn = updatedOn;
    }

    public LocalDateTime getCreatedAt() {
        return createUpdate.createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createUpdate.createdAt = createdAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @PrePersist
    public void onPreparePersist() {
        version = 1L;
        createUpdate.createdAt = LocalDateTime.now();
    }

    @PreUpdate
    public void onPrepareUpdate() {
        createUpdate.updatedOn = LocalDateTime.now();
    }

    @PreRemove
    public void onRemove() { isDeleted = true; }
}
