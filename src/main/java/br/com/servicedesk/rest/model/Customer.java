package br.com.servicedesk.rest.model;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Collection;


@Entity
@Table(schema = "corporate", name = "customers")
public class Customer extends BaseEntity {

    @Id
    @SequenceGenerator(name = "sq_customers", sequenceName = "\"corporate\".customers_seq")
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    private String login;
    private String password;
    private boolean active;
    @Column(name = "token_expired")
    private boolean tokenExpired;
    private LocalDate birthday;

    @Pattern(regexp = "[0-9]+", message = "The id must be a valid number")
    @Column(name = "phone")
    @Size(min = 8, max = 15)
    private String phone;

    @Column(name = "department")
    @Size(min = 2, max = 50)
    private String department;

    @Column(name = "job_role")
    @Size(min = 2, max = 50)
    private String jobRole;

    //TODO at the moment lets keep commented to finished authentication
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="company_id", nullable=false)
//    private Company company;

    @ManyToMany
    @JoinTable(
            name = "customers_roles",
            schema = "corporate",
            joinColumns = @JoinColumn(
                    name = "customer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

//    public Company getCompany() {
//        return company;
//    }
//
//    public void setCompany(Company company) {
//        this.company = company;
//    }
}
