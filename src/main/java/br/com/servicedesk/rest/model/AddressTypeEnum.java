package br.com.servicedesk.rest.model;

public enum AddressTypeEnum {
    HOME_ADDRESS, BUSINESS_ADDRESS, MAILING_ADDRESS
}
