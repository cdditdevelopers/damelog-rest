package br.com.servicedesk.rest.validators;


import br.com.servicedesk.rest.web.dto.user.UserPasswordUpdateDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(final PasswordMatches constraintAnnotation) {
        //
    }

    @Override
    public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
        final UserPasswordUpdateDTO user = (UserPasswordUpdateDTO) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }

}