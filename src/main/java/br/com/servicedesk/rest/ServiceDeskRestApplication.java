package br.com.servicedesk.rest;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
//https://github.com/bslota/multi-login
//https://github.com/chathurangat/spring-boot-security-unit-test-example/blob/master/src/main/java/com/springbootdev/examples/security/basic/config/SpringSecurityConfig.java
@SpringBootApplication
public class ServiceDeskRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDeskRestApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
