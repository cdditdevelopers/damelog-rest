package br.com.servicedesk.rest.service;


import br.com.servicedesk.rest.mapper.UserPageableFilterDTOMapper;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.pageable.PageFilter;
import br.com.servicedesk.rest.repository.UserRepository;
import br.com.servicedesk.rest.web.dto.user.UserDTO;
import br.com.servicedesk.rest.web.dto.user.UserPageableFilterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserPageableFilterDTOMapper userPageableFilterDTOMapper;

//    @Autowired
//    private VerificationTokenRepository tokenRepository;


    @Override
    public User getUser(String verificationToken) {
//        final VerificationToken token = tokenRepository.findByToken(verificationToken);
//        if (token != null) {
//            return token.getUser();
//        }
        return null;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }


    public void test(PageFilter<UserPageableFilterDTO> pagefilter) {
        //TODO finalizar o exemplo de paginacao
        Example<User> userExample = userPageableFilterDTOMapper.from(pagefilter.getExample());
        Page<User> result = userRepository.findAll(userExample, createPageRequest(pagefilter));

//        Pageable p =result.nextPageable();
        // page pp = repository.findAll(p);


        //
        result.getTotalElements();
        result.getTotalPages();
        result.getContent();
        result.getSize();
        result.getNumber();
        result.getNumberOfElements();
        result.getPageable();
        result.getSort();
    }

    private Example<User> mapTo(UserPageableFilterDTO example) {
        return null;
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> getUserByID(long id) {
        return userRepository.findById(id);
    }

    @Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public void deleteById(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setDeleted(Boolean.TRUE);
            userRepository.save(user);
        } else {
            throw new EntityNotFoundException("There is no user with id: " + id);
        }
    }

    private Pageable createPageRequest(PageFilter<UserPageableFilterDTO> pagefilter) {
        PageRequest pageRequest = PageRequest.of(pagefilter.getPage(), pagefilter.getPageSize());
        pagefilter.getSorts().forEach(s -> pageRequest.getSort().and(new Sort(s.getDirection(), s.getPropertry())));
        return pageRequest;
    }

}
