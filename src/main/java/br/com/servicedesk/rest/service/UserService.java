package br.com.servicedesk.rest.service;


import br.com.servicedesk.rest.model.User;

import javax.validation.Valid;
import java.util.Optional;

public interface UserService {

    User getUser(String verificationToken);
    User saveUser(@Valid User user);

    User findUserByEmail(String email);
    Optional<User> getUserByID(long id);
    void changeUserPassword(User user, String password);

    void deleteById(long id);
}
