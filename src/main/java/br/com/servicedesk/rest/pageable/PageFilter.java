package br.com.servicedesk.rest.pageable;

import java.util.List;

public class PageFilter<T> {

    private List<FieldSort> sorts;
    private int pageSize;
    private int page;
    private T example;

    public List<FieldSort> getSorts() {
        return sorts;
    }

    public void setSorts(List<FieldSort> sorts) {
        this.sorts = sorts;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public T getExample() {
        return example;
    }

    public void setExample(T example) {
        this.example = example;
    }
}
