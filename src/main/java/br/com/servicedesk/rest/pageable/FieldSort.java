package br.com.servicedesk.rest.pageable;

import org.springframework.data.domain.Sort;

import java.io.Serializable;

public class FieldSort implements Serializable{

    private Sort.Direction direction;
    private String propertry;

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    public String getPropertry() {
        return propertry;
    }

    public void setPropertry(String propertry) {
        this.propertry = propertry;
    }
}
