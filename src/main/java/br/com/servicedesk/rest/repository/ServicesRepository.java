package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.Services;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface ServicesRepository extends JpaRepository<Services, Long>, QueryByExampleExecutor<Services> {
}
