package br.com.servicedesk.rest.repository;


import br.com.servicedesk.rest.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

//https://github.com/Baeldung/spring-security-registration/blob/master/src/main/java/org/baeldung/persistence/model/VerificationToken.java
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);

}