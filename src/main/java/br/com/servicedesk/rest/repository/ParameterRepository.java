package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path="parameters")
public interface ParameterRepository extends JpaRepository<Parameter, Long> {

    List<Parameter> findAll();
    List<Parameter> findByKeyContainingIgnoreCase(String key);
}
