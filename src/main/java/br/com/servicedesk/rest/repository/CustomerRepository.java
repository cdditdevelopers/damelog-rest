package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface CustomerRepository extends JpaRepository<Customer, Long>, QueryByExampleExecutor<Customer> {
    Customer findByEmail(String email);
    Customer findByLogin(String login);
    Customer findByLoginOrEmail(String login, String email);
    Customer findByLoginOrEmailAndActiveTrue(String login, String email);

    @Override
    void delete(Customer user);
}
