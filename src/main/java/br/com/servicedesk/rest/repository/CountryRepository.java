package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface CountryRepository extends JpaRepository<Country, Long>, QueryByExampleExecutor<Country> {
}
