package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Component;

@Component
public interface UserRepository extends JpaRepository<User, Long>, QueryByExampleExecutor<User> {
    User findByEmail(String email);
    User findByLogin(String login);
    User findByLoginOrEmail(String login, String email);
    User findByLoginOrEmailAndIsDeletedFalse(String login, String email);

    @Override
    void delete(User user);

    /*
    @Query("Select u from User where name like '%100'", nativeQuery= true)
    List<User> usersWith100InName();
     */
}