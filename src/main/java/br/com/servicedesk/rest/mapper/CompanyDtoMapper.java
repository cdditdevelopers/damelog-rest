package br.com.servicedesk.rest.mapper;

import br.com.servicedesk.rest.model.Company;
import br.com.servicedesk.rest.web.dto.company.CompanyDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompanyDtoMapper {

    @Autowired
    private ModelMapper modelMapper;


    public Company from(CompanyDTO dto) {
        Company model = modelMapper.map(dto, Company.class);
        return model;
    }

    public CompanyDTO from(Company model) {
        CompanyDTO dto = modelMapper.map(model, CompanyDTO.class);
        return dto;
    }
}
