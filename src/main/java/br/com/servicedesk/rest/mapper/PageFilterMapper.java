package br.com.servicedesk.rest.mapper;

import br.com.servicedesk.rest.pageable.FieldSort;
import br.com.servicedesk.rest.pageable.PageFilter;
import br.com.servicedesk.rest.web.dto.user.UserPageableFilterDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class PageFilterMapper {

    private Pageable createPageRequest(PageFilter pagefilter) {
        PageRequest pageRequest = new PageRequest(pagefilter.getPage(), pagefilter.getPageSize());

//        for (FieldSort fs : pagefilter.getSorts()) {
//
//        }
//
//        pagefilter.getSorts().forEach(s -> pageRequest.getSort().and(new Sort(s.getDirection(), s.getPropertry())));
        return pageRequest;
    }
}
