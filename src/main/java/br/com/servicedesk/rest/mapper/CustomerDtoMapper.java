package br.com.servicedesk.rest.mapper;

import br.com.servicedesk.rest.model.Customer;
import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.web.dto.customer.CustomerDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CustomerDtoMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Customer from(CustomerDTO dto) {
        Customer model = modelMapper.map(dto, Customer.class);
        model.setBirthday(dto.getBirthday());
        model.setPassword(new BCryptPasswordEncoder().encode("servicedesk@123"));
        Set<Role> roles = dto.getRolesDTO().stream().map(
                r -> new Role(r)
        ).collect(Collectors.toSet());
        model.getRoles().addAll(roles);
        model.setActive(Boolean.TRUE);
        return model;
    }

    public CustomerDTO from(Customer model) {
        CustomerDTO dto = modelMapper.map(model, CustomerDTO.class);
        dto.setBirthday(model.getBirthday());
        return dto;
    }
}
