package br.com.servicedesk.rest.mapper;

import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.web.dto.user.UserPageableFilterDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserPageableFilterDTOMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Example<User> from(UserPageableFilterDTO dto) {
        User model = modelMapper.map(dto, User.class);
//        model.setBirthday(dto.getBirthday());
        Set<Role> roles = dto.getRolesDTO().stream().map(
                r -> new Role(r)
        ).collect(Collectors.toSet());
        model.getRoles().addAll(roles);

        return Example.of(model);
    }
}
