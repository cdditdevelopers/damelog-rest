package br.com.servicedesk.rest.mapper;

import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.web.dto.user.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserDtoMapper {

    @Autowired
    private ModelMapper modelMapper;

    public User from(UserDTO dto) {
        User model = modelMapper.map(dto, User.class);
        model.setBirthday(dto.getBirthday());
        model.setPassword(new BCryptPasswordEncoder().encode("servicedesk@123"));
        Set<Role> roles = dto.getRolesDTO().stream().map(
                r -> new Role(r)
        ).collect(Collectors.toSet());
        model.getRoles().addAll(roles);
        model.setDeleted(Boolean.FALSE);
        return model;
    }

    public UserDTO from(User model) {
        UserDTO dto = modelMapper.map(model, UserDTO.class);
        dto.setBirthday(model.getBirthday());
        return dto;
    }
}
