package br.com.servicedesk.rest.web.dto;

import org.springframework.data.domain.Sort;

import java.io.Serializable;

public class FilterPageable<T> implements Serializable {

    private Sort.Direction direction;
    private String fieldname;
    private T example;

    public FilterPageable(Sort.Direction direction, String fieldname) {
        this.direction = direction;
        this.fieldname = fieldname;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public T getExample() {
        return example;
    }

    public void setExample(T example) {
        this.example = example;
    }
}
