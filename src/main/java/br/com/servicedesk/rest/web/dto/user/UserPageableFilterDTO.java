package br.com.servicedesk.rest.web.dto.user;

import java.time.LocalDate;
import java.util.Set;

public class UserPageableFilterDTO {

    private String firstName;
    private String lastName;
    private String email;
    private LocalDate birthday;
    private Set<Long> rolesDTO;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Set<Long> getRolesDTO() {
        return rolesDTO;
    }

    public void setRolesDTO(Set<Long> rolesDTO) {
        this.rolesDTO = rolesDTO;
    }
}
