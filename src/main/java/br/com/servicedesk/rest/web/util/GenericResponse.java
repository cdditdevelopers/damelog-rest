package br.com.servicedesk.rest.web.util;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.nio.charset.Charset;
import java.util.List;

public class GenericResponse {

    private String message;
    private String error;

    public GenericResponse(final String message) {
        super();
        this.message = message;
    }

    public GenericResponse(final String message, final String error) {
        super();
        this.message = message;
        this.error = error;
    }

    public GenericResponse(List<ObjectError> allErrors, String error) {
        this.error = error;

        message = "";
        for (ObjectError objectError : allErrors) {

            if (objectError instanceof FieldError) {
                message += "{field:" + ((FieldError) objectError).getField() + ",defaultMessage:" + objectError.getDefaultMessage() + "}";
            } else {
                message +=  "{object:" + objectError.getObjectName() + ",defaultMessage:" + objectError.getDefaultMessage() + "}";
            }
        }

//        String temp = allErrors.stream().map(e -> {
//            if (e instanceof FieldError) {
//                return "{\"field\":\"" + ((FieldError) e).getField() + "\",\"defaultMessage\":\"" + e.getDefaultMessage() + "\"}";
//            } else {
//                return "{\"object\":\"" + e.getObjectName() + "\",\"defaultMessage\":\"" + e.getDefaultMessage() + "\"}";
//            }
//        }).collect(Collectors.joining(","));
        this.message =  new String(("[" + message  + "]").getBytes(), Charset.forName("UTF-8"));
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(final String error) {
        this.error = error;
    }

}