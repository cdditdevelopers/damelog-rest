package br.com.servicedesk.rest.web.security;

public interface ISecurityUserService {
    String validatePasswordResetToken(long id, String token);
}