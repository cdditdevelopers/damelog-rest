package br.com.servicedesk.rest.web.jwt;

import br.com.servicedesk.rest.model.Customer;
import br.com.servicedesk.rest.model.Privilege;
import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.repository.CustomerRepository;
import br.com.servicedesk.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("jwtCustomerDetails")
@Transactional
public class JwtCustomerDetails implements UserDetailsService {

    @Autowired
    private CustomerRepository repo;


    @Override
    public UserDetails loadUserByUsername(final String emailOrEmail) throws UsernameNotFoundException {
        try {
            final Customer c = repo.findByLoginOrEmail(emailOrEmail, emailOrEmail);
            if (c == null) {
                throw new UsernameNotFoundException("No user found with username: " + emailOrEmail);
            }

            return new org.springframework.security.core.userdetails.User(c.getEmail(),
                                                                          c.getPassword(),
                                                                          c.isActive(),
                                                          true,
                                                        true,
                                                           true,
                                                                           getAuthorities(c.getRoles()));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private final Collection<? extends GrantedAuthority> getAuthorities(final Collection<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }

    private final List<String> getPrivileges(final Collection<Role> roles) {
        final List<String> privileges = new ArrayList<String>();
        final List<Privilege> collection = new ArrayList<Privilege>();
        for (final Role role : roles) {
            collection.addAll(role.getPrivileges());
        }
        for (final Privilege item : collection) {
            privileges.add(item.getName());
        }

        return privileges;
    }


    private final List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
        final List<GrantedAuthority> authorities = new ArrayList<>();
        for (final String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + privilege));
        }
        return authorities;
    }
}
