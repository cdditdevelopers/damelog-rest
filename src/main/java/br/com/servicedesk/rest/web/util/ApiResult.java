package br.com.servicedesk.rest.web.util;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ApiResult<T extends Serializable> {

    private List<ApiErrorItem> errors = new ArrayList<>();
    private T data;

    public ApiResult(List<ApiErrorItem> errors, T data) {
        this.errors = errors;
        this.data = data;
    }

    public ApiResult() {
    }

    public ApiResult(List<ObjectError> allErrors, String error) {

        for (ObjectError objectError : allErrors) {
            if (objectError instanceof FieldError) {
                errors.add(new ApiErrorItem(((FieldError) objectError).getField(), objectError.getDefaultMessage()));
//                message += "{field:" + ((FieldError) objectError).getField() + ",defaultMessage:" + objectError.getDefaultMessage() + "}";
            } else {
                errors.add(new ApiErrorItem(objectError.getObjectName(), objectError.getDefaultMessage()));
//                message +=  "{object:" + objectError.getObjectName() + ",defaultMessage:" + objectError.getDefaultMessage() + "}";
            }

        }
    }

    public List<ApiErrorItem> getErrors() {
        return errors;
    }

    public T getData() {
        return data;
    }
}
