package br.com.servicedesk.rest.web.controller;

import br.com.servicedesk.rest.mapper.UserDtoMapper;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.service.UserService;
import br.com.servicedesk.rest.web.dto.user.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/*
    validation
    test
    log
    auditoria
 */
@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "${frontend.admin.url}")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDtoMapper mapper;

    @GetMapping("/profile")
    public ResponseEntity<UserDTO> getById() {

        UsernamePasswordAuthenticationToken upat = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User userDetails =  (org.springframework.security.core.userdetails.User) upat.getPrincipal();
        System.out.println(userDetails.getUsername());

        //peggar do token!!!!
        long id = 1l;
        User user = userService.findUserByEmail(userDetails.getUsername());
        if (user != null) {
            return new ResponseEntity(mapper.from(user), HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getById(@PathVariable long id) {
        Optional<User> userByID = userService.getUserByID(id);
        if (userByID.isPresent() ) {
            User user = userByID.get();
            return new ResponseEntity(mapper.from(user), HttpStatus.OK);
        }
        return ResponseEntity.notFound().build();

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        userService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/")
    public ResponseEntity<UserDTO> update(@Valid @RequestBody UserDTO userDTO){
        User user = userService.saveUser(mapper.from(userDTO));
        return new ResponseEntity(mapper.from(user), HttpStatus.OK);
    }


    @PostMapping("/")
    public ResponseEntity<UserDTO> create(@Valid @RequestBody UserDTO userDTO){
        User user = userService.saveUser(mapper.from(userDTO));
        return new ResponseEntity(mapper.from(user), HttpStatus.OK);
    }
}
