package br.com.servicedesk.rest.web.dto.company;

import br.com.servicedesk.rest.validators.ValidEmail;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class CompanyDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3, max = 50, message = "{Size.companyDto.email}")
    private String corporateName;

    @NotNull
    @Size(min = 14, max = 50, message = "{Size.companyDto.cnpj}")
    private String cnpj;

    @NotNull
    @Size(min = 3, max = 50, message = "{Size.companyDto.stateRegistration}")
    private String stateRegistration;

    @NotNull
    @Size(min = 8, max = 15, message = "{Size.companyDto.phone}")
    private String phone;

    @ValidEmail
    @NotNull
    @Size(min = 4, max = 70, message = "{Size.companyDto.email}")
    private String email;

    @NotNull
    private Long managerId;

    @NotNull
    @Digits(integer = 10, fraction = 2, message = "{Size.companyDto.amountMonthlyContract}")
    private BigDecimal amountMonthlyContract;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate contractDateStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate contractDateEnd;

    @NotNull
    private String businessBranch;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getStateRegistration() {
        return stateRegistration;
    }

    public void setStateRegistration(String stateRegistration) {
        this.stateRegistration = stateRegistration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public BigDecimal getAmountMonthlyContract() {
        return amountMonthlyContract;
    }

    public void setAmountMonthlyContract(BigDecimal amountMonthlyContract) {
        this.amountMonthlyContract = amountMonthlyContract;
    }

    public LocalDate getContractDateStart() {
        return contractDateStart;
    }

    public void setContractDateStart(LocalDate contractDateStart) {
        this.contractDateStart = contractDateStart;
    }

    public LocalDate getContractDateEnd() {
        return contractDateEnd;
    }

    public void setContractDateEnd(LocalDate contractDateEnd) {
        this.contractDateEnd = contractDateEnd;
    }

    public String getBusinessBranch() {
        return businessBranch;
    }

    public void setBusinessBranch(String businessBranch) {
        this.businessBranch = businessBranch;
    }
}
