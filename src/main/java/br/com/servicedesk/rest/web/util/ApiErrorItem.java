package br.com.servicedesk.rest.web.util;

import java.io.Serializable;

public class ApiErrorItem implements Serializable {
    private String field;
    private String msg;

    public ApiErrorItem(String field, String msg) {
        this.field = field;
        this.msg = msg;
    }

    public String getField() {
        return field;
    }

    public String getMsg() {
        return msg;
    }
}
