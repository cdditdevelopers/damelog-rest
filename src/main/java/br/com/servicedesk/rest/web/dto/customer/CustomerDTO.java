 package br.com.servicedesk.rest.web.dto.customer;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

public class CustomerDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String firstName;

    @NotNull
    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String lastName;

    @NotNull
    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String email;

    @NotNull
    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String login;

    private boolean active;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate birthday;


    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String phone;

    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String department;

    @Size(min = 1, max= 10, message = "{Size.customerDto.firstName}")
    private String jobRole;

    @NotEmpty
    private Set<Long> rolesDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public Set<Long> getRolesDTO() {
        return rolesDTO;
    }

    public void setRolesDTO(Set<Long> rolesDTO) {
        this.rolesDTO = rolesDTO;
    }
}
