package br.com.servicedesk.rest.web.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class JWTWebSecurityConfig {


	@Bean
	public PasswordEncoder passwordEncoderBean() {
		return new BCryptPasswordEncoder();
	}

	@Configuration
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
	@Order(2)
	public static class CustomerSecurityConfig extends WebSecurityConfigurerAdapter {

		@Autowired
		private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

		@Qualifier("jwtUserDetailsDamelog")
		@Autowired
		private UserDetailsService jwtUserDetailsDamelog;

		@Autowired
		private JwtTokenAuthorizationOncePerRequestFilter jwtAuthenticationTokenFilter;

		@Value("${jwt.get.token.uri}")
		private String authenticationPath;

		@Autowired
		private PasswordEncoder passwordEncoder;

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(jwtUserDetailsDamelog).passwordEncoder(passwordEncoder);
		}

		@Qualifier("customer")
		@Bean
		public AuthenticationManager authenticationManagerBean2() throws Exception {
			return super.authenticationManagerBean();
		}


		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			httpSecurity.csrf().disable().exceptionHandling()
					.authenticationEntryPoint(jwtUnAuthorizedResponseAuthenticationEntryPoint)
					.and()
					.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and()
					.antMatcher("/")
					.authorizeRequests().anyRequest()
					.authenticated();

			httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

			httpSecurity.headers().frameOptions().sameOrigin() // H2 Console Needs this setting
					.cacheControl(); // disable caching
		}

		@Override
		public void configure(WebSecurity webSecurity) throws Exception {
			webSecurity.ignoring()
					.antMatchers(HttpMethod.POST, authenticationPath)
					.antMatchers(HttpMethod.OPTIONS, "/**")
					.antMatchers(HttpMethod.GET, "/");
		}

	}

	@Configuration
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
	@Order(1)
	public class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

		@Autowired
		private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

		@Qualifier("jwtUserDetailsDamelog")
		@Autowired
		private UserDetailsService jwtUserDetailsDamelog;

		@Autowired
		private JwtTokenAuthorizationOncePerRequestFilter jwtAuthenticationTokenFilter;


		@Value("${jwt.get.token.admin.uri}")
		private String adminAuthenticationPath;

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(jwtUserDetailsDamelog).passwordEncoder(passwordEncoderBean());
		}

		@Qualifier("admin")
		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			httpSecurity.csrf().disable().exceptionHandling()
					.authenticationEntryPoint(jwtUnAuthorizedResponseAuthenticationEntryPoint)
					.and()
					.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
					.and().antMatcher("/admin").authorizeRequests().anyRequest()
					.authenticated();

			httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

			httpSecurity.headers().frameOptions().sameOrigin() // H2 Console Needs this setting
					.cacheControl(); // disable caching
		}

		@Override
		public void configure(WebSecurity webSecurity) throws Exception {
			webSecurity.ignoring()
					.antMatchers(HttpMethod.POST, adminAuthenticationPath)
					.antMatchers(HttpMethod.OPTIONS, "/admin/**")
					.antMatchers(HttpMethod.GET, "/admin");
		}
	}
}