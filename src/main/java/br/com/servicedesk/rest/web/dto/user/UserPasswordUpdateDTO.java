package br.com.servicedesk.rest.web.dto.user;

import br.com.servicedesk.rest.validators.PasswordMatches;
import br.com.servicedesk.rest.validators.ValidEmail;
import br.com.servicedesk.rest.validators.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@PasswordMatches
public class UserPasswordUpdateDTO implements Serializable {

    @NotNull
    private String login;

    @ValidPassword
    private String password;

    @NotNull
    @Size(min = 1)
    private String matchingPassword;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }
}
