



/* admin */
INSERT INTO "corporate".users (version, active, first_name,        last_name,      login,       email,                 birthday,                           phone,         password,                                                       creation_time, token_expired)
                       VALUES (1,       true,   'Marcos Henrique', 'Barros Pinto', 'marcoscba', 'marcoscba@gmail.com', to_date('27/07/1982','DD/MM/YYYY'), '11985733131', '$2a$10$/tlT5420fO/AufJEhlBCGell5o/3aBnqp7C9UxpgAckcTJeQd9b7O', NOW(),         true);

INSERT INTO "corporate".users (version, active, first_name,        last_name,      login,         email,                 birthday,                           phone,         password,                                                       creation_time, token_expired)
                       VALUES (1,       true,   'Joao',            'Silva',        'joao.silva', 'joaosilva@gmail.com',  to_date('27/07/1982','DD/MM/YYYY'), '11985733131', '$2a$10$/tlT5420fO/AufJEhlBCGell5o/3aBnqp7C9UxpgAckcTJeQd9b7O', NOW(),         true);

INSERT INTO "corporate".privileges (version, name, creation_time) values (1,'ADMIN', NOW());
INSERT INTO "corporate".privileges (version, name, creation_time) values (1,'OPERATOR', NOW());
INSERT INTO "corporate".privileges (version, name, creation_time) values (1,'MANAGER', NOW());
INSERT INTO "corporate".privileges (version, name, creation_time) values (1,'CUSTOMER', NOW(), NOW());


INSERT INTO "corporate".roles (version, name, creation_time) values (1,'ADMIN', NOW());
INSERT INTO "corporate".roles (version, name, creation_time) values (1,'OPERATOR', NOW(), NOW());
INSERT INTO "corporate".roles (version, name, creation_time) values (1,'MANAGER', NOW(), NOW());
INSERT INTO "corporate".roles (version, name, creation_time) values (1,'CUSTOMER', NOW(), NOW());

INSERT INTO "corporate".users_roles values(4,5);
INSERT INTO "corporate".privileges (version, name, creation_time) values (1,'ADMIN', NOW());
INSERT INTO "corporate".roles_privileges values(5,4);

/*
INSERT INTO "corporate".customers()

INSERT INTO "corporate".users_roles()
INSERT INTO "corporate".roles_privileges()
INSERT INTO "corporate".customers_roles()
*/
--brasil
INSERT INTO "corporate".countries(id, name, acronym) values (1, 'Brasil', 'BR');


INSERT INTO "corporate".states(id, name, acronym, country_id) values (1, 'Acre', 'AC', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (2, 'Alagoas', 'AL', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (3, 'Amazonas', 'AM', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (4, 'Amapa', 'AP', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (5, 'Bahia', 'BA', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (6, 'Ceará', 'CE', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (7, 'Distrito Federal', 'DF', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (8, 'Espirito Santo', 'ES', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (9, 'Goias', 'GO', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (10, 'Maranhao', 'MA', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (11, 'Minas Gerais', 'MG', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (12, 'Mato Grosso do Sul', 'MS', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (13, 'Mato Grosso', 'MT', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (14, 'Para', 'PA', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (15, 'Paraiba', 'PB', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (16, 'Pernambuco', 'PE', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (17, 'Piaui', 'PI', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (18, 'Parana', 'PR', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (19, 'Rio de Janeiro', 'RJ', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (20, 'Rio Grande do Norte', 'RN', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (21, 'Rondonia', 'RO', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (22, 'Roraima', 'RR', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (23, 'Rio Grande do Sul', 'RS', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (24, 'Santa Catarina', 'SC', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (25, 'Sergipe', 'SE', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (26, 'Sao Paulo', 'SP', 1);
INSERT INTO "corporate".states(id, name, acronym, country_id) values (27, 'Tocantins', 'TO', 1);