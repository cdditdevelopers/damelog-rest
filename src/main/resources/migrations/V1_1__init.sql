
/*
cria o arquivo de migracao na mao msm!!!
  mvn flyway:baseline
  mvn flyway:info
  mvn flyway:migrate
  mvn flyway:clean
 */


/**
USERS
**/
CREATE TABLE IF NOT EXISTS "corporate".users (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	is_deleted BOOLEAN NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	login VARCHAR(50) UNIQUE NOT NULL,
	email VARCHAR(70) UNIQUE NOT NULL,
	birthday DATE NOT NULL,
	phone VARCHAR(15),
	password VARCHAR(200) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP,
	token_expired BOOLEAN NOT NULL
);

CREATE SEQUENCE "corporate".users_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".users ALTER COLUMN id SET DEFAULT nextval('"corporate".users_seq');
ALTER SEQUENCE "corporate".users_seq OWNED BY "corporate".users.id;

CREATE INDEX index_users_first_name ON "corporate".users (first_name);

/**
ROLES
**/
CREATE TABLE IF NOT EXISTS "corporate".roles (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "corporate".users_roles (
	user_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	PRIMARY KEY(user_id, role_id),
	CONSTRAINT fk_users_roles_to_users FOREIGN KEY(user_id) REFERENCES "corporate".users(id),
	CONSTRAINT fk_users_roles_to_roles FOREIGN KEY(role_id) REFERENCES "corporate".roles(id)
);

CREATE SEQUENCE "corporate".roles_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "corporate".roles ALTER COLUMN id SET DEFAULT nextval('"corporate".roles_seq');

ALTER SEQUENCE "corporate".roles_seq OWNED BY "corporate".roles.id;

/**
PRIVILEGES
**/
CREATE TABLE IF NOT EXISTS "corporate".privileges (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".privileges_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".privileges ALTER COLUMN id SET DEFAULT nextval('"corporate".privileges_seq');
ALTER SEQUENCE "corporate".privileges_seq OWNED BY "corporate".privileges.id;

CREATE TABLE IF NOT EXISTS "corporate".roles_privileges (
	role_id BIGINT NOT NULL,
	privilege_id BIGINT NOT NULL,
	PRIMARY KEY(role_id, privilege_id),
	CONSTRAINT fk_roles_privileges_to_roles FOREIGN KEY(role_id) REFERENCES "corporate".roles(id),
	CONSTRAINT fk_roles_privileges_to_privileges  FOREIGN KEY(privilege_id)  REFERENCES "corporate".privileges(id)
);




/**
COUNTRY
**/
CREATE TABLE IF NOT EXISTS "corporate".countries (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	acronym VARCHAR(50) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".countries_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".countries ALTER COLUMN id SET DEFAULT nextval('"corporate".countries_seq');
ALTER SEQUENCE "corporate".countries_seq OWNED BY "corporate".countries.id;

/**
STATES
**/

CREATE TABLE IF NOT EXISTS "corporate".states (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	acronym VARCHAR(50) NOT NULL,
	country_id BIGINT NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".states_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".states ALTER COLUMN id SET DEFAULT nextval('"corporate".states_seq');
ALTER SEQUENCE "corporate".states_seq OWNED BY "corporate".states.id;

/**
CITIES
**/

CREATE TABLE IF NOT EXISTS "corporate".cities (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	acronym VARCHAR(50) NOT NULL,
	state_id BIGINT NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".cities_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".cities ALTER COLUMN id SET DEFAULT nextval('"corporate".cities_seq');
ALTER SEQUENCE "corporate".cities_seq OWNED BY "corporate".cities.id;

/**
BUSINESS_SEGMENT
**/
CREATE TABLE IF NOT EXISTS "corporate".business_segment (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".business_segment_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".business_segment ALTER COLUMN id SET DEFAULT nextval('"corporate".business_segment_seq');
ALTER SEQUENCE "corporate".business_segment_seq OWNED BY "corporate".business_segment.id;

/**
SERVICES
**/
CREATE TABLE IF NOT EXISTS "corporate".services (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	name VARCHAR(50) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP
);

CREATE SEQUENCE "corporate".services_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".services ALTER COLUMN id SET DEFAULT nextval('"corporate".services_seq');
ALTER SEQUENCE "corporate".services_seq OWNED BY "corporate".services.id;


/**
CUSTOMERS
**/
CREATE TABLE IF NOT EXISTS "corporate".customers (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(70) UNIQUE NOT NULL,
	login VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(200) NOT NULL,
	is_deleted BOOLEAN NOT NULL,
	token_expired BOOLEAN NOT NULL,
	birthday DATE NOT NULL,
	phone VARCHAR(15),
	department VARCHAR(200) NOT NULL,
	job_role VARCHAR(200) NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP,
  company_id BIGINT
);

CREATE SEQUENCE "corporate".customers_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".customers ALTER COLUMN id SET DEFAULT nextval('"corporate".customers_seq');
ALTER SEQUENCE "corporate".customers_seq OWNED BY "corporate".customers.id;

CREATE TABLE IF NOT EXISTS "corporate".customers_roles (
	customer_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	PRIMARY KEY(customer_id, role_id),
	CONSTRAINT fk_customers_roles_to_customers FOREIGN KEY(customer_id) REFERENCES "corporate".customers(id),
	CONSTRAINT fk_customers_roles_to_roles FOREIGN KEY(role_id) REFERENCES "corporate".roles(id)
);



/**
COMPANIES
**/
CREATE TABLE IF NOT EXISTS "corporate".companies (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	is_deleted BOOLEAN NOT NULL,
	corporate_name VARCHAR(50) NOT NULL,
	cnpj VARCHAR(20) NOT NULL,
	state_registration VARCHAR(50) NOT NULL,
  phone VARCHAR(15),
	email VARCHAR(70) UNIQUE NOT NULL,
	manager_id BIGINT NOT NULL,
	creation_time TIMESTAMP NOT NULL,
	modification_time TIMESTAMP,
	amount_monthly_contract NUMERIC (10, 2),
  contract_date_start DATE,
  contract_date_end DATE,
  business_branch VARCHAR(50)
);

CREATE SEQUENCE "corporate".companies_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".companies ALTER COLUMN id SET DEFAULT nextval('"corporate".companies_seq');
ALTER SEQUENCE "corporate".companies_seq OWNED BY "corporate".companies.id;

ALTER TABLE "corporate".customers ADD CONSTRAINT fk_customer_to_company FOREIGN KEY (company_id) REFERENCES "corporate".companies(id);

/**
ADDRESSES
**/
CREATE TABLE IF NOT EXISTS "corporate".addresses (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	zip VARCHAR(50) NOT NULL,
	place VARCHAR(300) NOT NULL,
	neighbourhood VARCHAR(300) NOT NULL,
	number_address VARCHAR(20) NOT NULL,
  city_id BIGINT,
  company_id BIGINT,
  address_type VARCHAR(300) NOT NULL,
  complement VARCHAR(300) NOT NULL
);

CREATE SEQUENCE "corporate".addresses_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "corporate".addresses ALTER COLUMN id SET DEFAULT nextval('"corporate".addresses_seq');
ALTER SEQUENCE "corporate".addresses_seq OWNED BY "corporate".addresses.id;

ALTER TABLE "corporate".addresses ADD CONSTRAINT fk_address_to_city FOREIGN KEY (city_id) REFERENCES "corporate".cities(id);
ALTER TABLE "corporate".addresses ADD CONSTRAINT fk_address_to_company FOREIGN KEY (company_id) REFERENCES "corporate".companies(id);


/**
INVOICES
**/
CREATE TABLE IF NOT EXISTS "corporate".invoices (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	number_invoice VARCHAR(50) NOT NULL,
	due_date DATE NOT NULL,
	amount NUMERIC (10, 2) NOT NULL,
	status  VARCHAR(50) NOT NULL,
	company_id BIGINT NOT NULL
);

CREATE SEQUENCE "corporate".invoices_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "corporate".invoices ALTER COLUMN id SET DEFAULT nextval('"corporate".invoices_seq');
ALTER SEQUENCE "corporate".invoices_seq OWNED BY "corporate".invoices.id;

ALTER TABLE "corporate".invoices ADD CONSTRAINT fk_invoice_to_company FOREIGN KEY (company_id) REFERENCES "corporate".companies(id);

