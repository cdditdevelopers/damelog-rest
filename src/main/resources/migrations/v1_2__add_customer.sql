



CREATE TABLE IF NOT EXISTS "corporate".customers (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	modification_time TIMESTAMP,
	creation_time TIMESTAMP NOT NULL,
  first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(70) UNIQUE NOT NULL,
	login VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(200) NOT NULL,
	is_deleted BOOLEAN NOT NULL,
	token_expired BOOLEAN not null,
	birthday DATE NOT NULL,
	phone VARCHAR(15),
	department VARCHAR(50),
	job_role VARCHAR(50)
);


CREATE SEQUENCE "corporate".customers_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "corporate".customers ALTER COLUMN id SET DEFAULT nextval('"corporate".customers_seq');
ALTER SEQUENCE "corporate".customers_seq OWNED BY "corporate".customers.id;



CREATE TABLE IF NOT EXISTS "corporate".customers_roles (
	customer_id BIGINT NOT NULL,
	role_id BIGINT NOT NULL,
	PRIMARY KEY(customer_id, role_id),
	CONSTRAINT fk_customers_roles_to_users FOREIGN KEY(customer_id) REFERENCES "corporate".customers(id),
	CONSTRAINT fk_customers_roles_to_roles FOREIGN KEY(role_id) REFERENCES "corporate".roles(id)
);



INSERT INTO "corporate".customers
(version,
 creation_time,
 first_name,
 last_name,
 email,
 login,
 password,
 is_deleted,
 token_expired,
 birthday,
 phone,
 department,
 job_role)
values

(1,
now(),
'Rodrigo',
'Candido',
'rcandido@gmail.com',
'rcandido',
'$2a$10$/tlT5420fO/AufJEhlBCGell5o/3aBnqp7C9UxpgAckcTJeQd9b7O',
true,
true,
to_date('27/07/1982','DD/MM/YYYY'),
'11985733131',
'Sales',
'Manager'
);
