



CREATE TABLE IF NOT EXISTS "corporate".parameters (
  id BIGSERIAL PRIMARY KEY,
	version BIGINT NOT NULL,
	is_deleted BOOLEAN NOT NULL,
	modification_time TIMESTAMP,
	creation_time TIMESTAMP NOT NULL,
  value_param VARCHAR(5000) NOT NULL,

	key_param VARCHAR(50) NOT NULL
);


CREATE SEQUENCE "corporate".parameters_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "corporate".parameters ALTER COLUMN id SET DEFAULT nextval('"corporate".parameters_seq');
ALTER SEQUENCE "corporate".parameters_seq OWNED BY "corporate".parameters.id;




INSERT INTO "corporate".parameters
(version,
 creation_time,
 key_param,
 	is_deleted,
 value_param)
values
(1,
now(),
'PRINTER_URL',
 false,
'\\123.231.321\docs'
);
