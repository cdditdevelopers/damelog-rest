package br.com.servicedesk.rest.web.controller;

import br.com.servicedesk.rest.mapper.UserDtoMapper;
import br.com.servicedesk.rest.repository.UserRepository;
import br.com.servicedesk.rest.service.UserService;
import br.com.servicedesk.rest.web.jwt.JWTWebSecurityConfig;
import br.com.servicedesk.rest.web.jwt.JwtTokenUtil;
import br.com.servicedesk.rest.web.jwt.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import br.com.servicedesk.rest.web.jwt.JwtUserDetailsDamelog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
//@WebMvcTest(UserController.class)
//@Import({JWTWebSecurityConfig.class,
//        JwtUnAuthorizedResponseAuthenticationEntryPoint.class,
//        JwtUserDetailsDamelog.class,
//        JwtTokenUtil.class})
public class UserControllerTests {
//https://www.concretepage.com/spring-4/spring-4-security-junit-test-with-withmockuser-and-withuserdetails-annotation-example-using-webappconfiguration
    @Autowired
    MockMvc mvc;

    @MockBean
    UserService service;

    @MockBean
    UserDtoMapper mapper;

    @MockBean
    UserRepository repoo;

//    @Test
    public void findUserById() throws Exception {

        br.com.servicedesk.rest.model.User alex = new br.com.servicedesk.rest.model.User();
        alex.setEmail("corporate");
        alex.setFirstName("Alex");

        List<br.com.servicedesk.rest.model.User> allEmployees = Arrays.asList(alex);

        given(service.getUserByID(anyLong())).willReturn(Optional.of(alex));

        mvc.perform(get("/usersv2/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
//                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

//    @EnableWebSecurity
//    @EnableWebMvc
//    static class Config extends WebSecurityConfigurerAdapter {
//        // @formatter:off
//        @Bean
//        public UserDetailsService userDetailsService() {
//            UserDetails user = User.withDefaultPasswordEncoder().username("user").password("password").roles("ADMIN").build();
//            return new InMemoryUserDetailsManager(user);
//        }
//        // @formatter:on
//    }
}
