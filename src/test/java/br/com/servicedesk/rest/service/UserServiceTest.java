package br.com.servicedesk.rest.service;

import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

//@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    UserService userService;

    @MockBean
    UserRepository repo;

    @MockBean
    PasswordEncoder passwordEncoder;

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

//    @Test
    public void find() {
        User u = new User();
        u.setFirstName("Marcos barroos");
        given(repo.findById(any())).willReturn(Optional.of(u));
        Optional<User> userByID = userService.getUserByID(1L);
        Assert.assertTrue(userByID.isPresent());
    }
}
