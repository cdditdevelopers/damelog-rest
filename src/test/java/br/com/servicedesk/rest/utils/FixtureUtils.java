package br.com.servicedesk.rest.utils;

import br.com.six2six.fixturefactory.function.AtomicFunction;

import java.time.LocalDate;

public class FixtureUtils {

    public static AtomicFunction localDate() {
        return new AtomicFunction() {
            @Override
            public <T> T generateValue() {
                return (T) LocalDate.now();
            }
        };
    }
}
