package br.com.servicedesk.rest.repository;

import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.model.User;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTest {

    @Autowired
    UserRepository repo;

    @Autowired
    RoleRepository repoRole;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("br.com.servicedesk.rest.fixture");
    }

    @Test
    public void fetchFromDB() {
        Optional<User> userOptional = repo.findById(1L);
        System.out.println(userOptional);
//        Assert.assertNotNull("nao pode ser nulo", userOptional);
//        Assert.assertFalse("tem q ser nulo pois nao existe", userOptional.isPresent());
        Role role = Fixture.from(Role.class).gimme("brandNew");
        Role roleSaved = repoRole.save(role);
        Assert.assertNotNull("role nao pode ser nulo", roleSaved);
        Assert.assertNotNull("role must have id", roleSaved.getId());

        User user = Fixture.from(User.class).gimme("brandNew");
        user.getRoles().add(role);
        User userSaved = repo.save(user);
        Assert.assertNotNull("user nao pode ser nulo", userSaved);
        Assert.assertNotNull("user must have id", userSaved.getId());

//        Optional<User> userTobEDeactivated = repo.findById(userSaved.getId());
//        User u = userTobEDeactivated.get();
//        u.setActive(Boolean.TRUE);
//        repo.saveAndFlush(u);
//        User userNotFound = repo.findByLoginOrEmailAndActiveTrue(user.getEmail(), user.getLogin());
//        Assert.assertNull("user deve ser nulo", userNotFound);

    }

//    @Test
    public void pagination() {
        List<User> users = Fixture.from(User.class).gimme(50, "brandNewUserForPagination");
        repo.saveAll(users);
        Example<User> userFilterExample = Example.of(Fixture.from(User.class).gimme("userFilterByFirstNameRodrigo"));
        PageRequest pageRequest = new PageRequest(0, 10);

        Page<User> all = repo.findAll(userFilterExample, pageRequest);
        System.out.println(all.getTotalElements());
        System.out.println("FIM OK");
    }

}
