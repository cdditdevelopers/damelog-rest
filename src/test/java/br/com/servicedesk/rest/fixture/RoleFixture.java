package br.com.servicedesk.rest.fixture;

import br.com.servicedesk.rest.model.Role;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class RoleFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Role.class)
                .addTemplate("brandNew", new Rule(){{
                    add("name", firstName());
                }}).addTemplate("admin", new Rule(){{
                    add("id", 1L);
                    add("name", "ADMIN");
                }}).addTemplate("operator", new Rule(){{
                    add("id", 2L);
                    add("name", "OPERATOR");
                }});
    }
}
