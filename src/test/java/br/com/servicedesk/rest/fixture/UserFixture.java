package br.com.servicedesk.rest.fixture;

import br.com.servicedesk.rest.model.Role;
import br.com.servicedesk.rest.model.User;
import br.com.servicedesk.rest.utils.FixtureUtils;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Random;

public class UserFixture implements TemplateLoader {

    @Override
    public void load() {
        Random r = new Random();

        Fixture.of(User.class)
                .addTemplate("brandNew", new Rule(){{
                    add("firstName", firstName());
                    add("lastName", lastName());
                    add("login", "mylogin" + r.nextInt());
                    add("active", Boolean.TRUE);
                    add("password", new BCryptPasswordEncoder().encode("servicedesk@123"));
                    add("email", "${login}@gmail.com");
                    add("birthday", FixtureUtils.localDate());
        //            add("address", one(Address.class, "valid"));
                }}).addTemplate("brandNewUserForPagination", new Rule(){{
                    add("firstName", random("Rodrigo" + r.nextInt(100), "Joao" + r.nextInt(100), "luiz" + r.nextInt(100)));
                    add("lastName", random("Silva" + r.nextInt(100), "Costa" + r.nextInt(100), "Coelho" + r.nextInt(100)));
                    add("login", "mylogin" + r.nextInt(500) + "_" + r.nextInt(500)); //este random nao funciona
                    add("active", Boolean.TRUE);
                    add("password", new BCryptPasswordEncoder().encode("servicedesk@123"));
                    add("email", "${login}@gmail.com");
                    add("birthday", FixtureUtils.localDate());
                    add("roles", has(1).of(Role.class, "operator"));
                }}).addTemplate("userFilterByFirstNameRodrigo", new Rule(){{
                    add("firstName", "Rodrigo");

                }});

    }
}
